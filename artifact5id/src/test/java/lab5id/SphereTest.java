//Chiril Barba - Test code for Sphere
package lab5id;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SphereTest {
    @Test
    public void testConstructor() {//Test the constructor
        Sphere sphere = new Sphere(4); //Creates a Sphere object
        assertEquals(4.0, sphere.getRadius(),0.001); 
    }
    @Test
    public void testGetVolume() {// Test for the getVolume() method
        Sphere sphere = new Sphere(4);
        double expectedVolume = (4 / 3) * Math.PI * Math.pow(4, 3);
        assertEquals(expectedVolume, sphere.getVolume(), 0.001); 
    }
    @Test
    public void testGetSurfaceArea() {//Test for the getSurfaceArea() method
        Sphere sphere = new Sphere(4);
        double expectedSurfaceArea = 4 * Math.PI * Math.pow(4.0, 2);
        assertEquals(expectedSurfaceArea, sphere.getSurfaceArea(),0.001);
    }
}