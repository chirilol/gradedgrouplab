package lab5id;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import lab5id.Cone;

public class ConeTest {
    @Test
    public void testGetHeight() {
        Cone cone = new Cone(5.0, 3.0); 
        assertEquals(5.0, cone.getHeight(),0.001);
    }

    @Test
    public void testGetRadius() {
        Cone cone = new Cone(5.0, 3.0);
        assertEquals(3.0, cone.getRadius(),0.001); 
    }

    @Test
    public void testGetVolume() {
        double expectedVolume = Math.PI * 3.0 * 3.0 * (5.0 / 3.0); 
        Cone cone = new Cone(5.0, 3.0); 
        assertEquals(expectedVolume, cone.getVolume(),0.001); 
    }

    @Test
    public void testGetArea() {
        double expectedArea = Math.PI * 3.0 * (3.0 + Math.sqrt((5.0 * 5.0) + (3.0 * 3.0))); 
        Cone cone = new Cone(5.0, 3.0);
        assertEquals(expectedArea, cone.getSurfaceArea(),0.001); 
    }
}

