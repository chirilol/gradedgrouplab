package lab5id;
//Chiril Barba
public class Cone implements Shape3d{
    private double height;
    private double radius;
    public Cone(double height, double radius){
        this.height = height;
        this.radius = radius;
    }
    public double getHeight(){
        return this.height;
    }
    public double getRadius(){
        return this.radius;
    }
    @Override
    public double getVolume() {
        return Math.PI * (radius*radius) * (height/3);
    }
    @Override
    public double getSurfaceArea() {
        return Math.PI*radius*(radius+Math.sqrt((height*height)+(radius*radius)));
    }
}
