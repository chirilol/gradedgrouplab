package lab5id;

public interface Shape3d {
    double getVolume();
    double getSurfaceArea();
}
